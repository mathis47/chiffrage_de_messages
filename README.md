# Projet 4 : Chiffrage de messages

Le but de ce programme est de chiffrer ou déchiffrer un message quelconque avec les algorithmes suivant:

* Algorithme de César
* Algorithme de Vigenère

## Utilisation
Entrez `make all` dans votre terminal linux en étant dans le dossier du projet et les instruction suivante devraient s'afficher
```bash
make all
gcc -c Chiffrer.c
gcc -c verifEtAccents.c
cc    -c -o ChiffrageMessage.o ChiffrageMessage.c
gcc Chiffrer.o verifEtAccents.o ChiffrageMessage.o -o main
```
Après pour executer le programme il suffi de faire `./main`

Pour effacer tout les fichers binaires et chiffrage.txt il suffi de faire `make clean`

Ne pas oublié d'installer le compilateur si ça ne fonctionne pas : `sudo apt-get install gcc`

## Affichage 
Vous entrez le message que vous voulez coder ou décoder
<pre>
    
---------------------
Chiffrage de messages
---------------------

Veuillez entrer un message : 
</pre>
Le message est enlevé de toutes accents puis on vous propose quel chiffrement voulez vous.
<pre>
Veuillez entrer un message : je suis là
Taille : 10 charactères (avec les espaces)
Message à chiffrer/déchiffrer : je suis la

Choisissez l'algorithme de chiffrement :
        1 - Chiffrement César
        2 - Chiffrement Vigenère
Choix ( 1 ou 2 ) :
</pre>
Vous pouvez choisir entre chiffrer ou déchiffrer.
<pre>
Choisissez entre Chiffrer et Déchiffrer :
        1 - Chiffrer
        2 - Déchiffrer
Choix ( 1 ou 2 ) :
</pre>
Un exemple avec les choix `1` puis `1`.Le programme vous demande ensuite la clé de cryptage ou de décryptage.
<pre>
---------------
Chiffrage Cesar
---------------

Clé (décalage) :
</pre>
Après cela l'affichage du cryptage ou décryptage se fait
<pre>
Clé (décalage) : 5

Message codé : oj xznx qf

Voulez vous avoir le résultat sur un fichier texte chiffrage.txt (o/n) : o
Le fichier chiffrage.txt a été créé ou complété

Merci de votre visite

Bescond Mathis

Colombier Korentin
</pre>
Vous avez la posibilité de choisir ou pas d'avoir le résultat dans un fichier chiffrage.txt.

Voici un exemple :

<pre>
---------------
Chiffrage Cesar
---------------
Clé : 5
Le message initial : je suis là
Le message codé : oj xznx qf
</pre>

Si vous répétez l'opération plusieurs fois les résultats pourront s'afficher les uns sous les autres.

## Fonctions (signatures) : 

#### Chiffrer.h :
```c
//Cette fonction permet de modifier le tableau de wchar_t pointé par chaine avec
//un chiffrage Vigenère et sa clé.
//La clé doit être une suite de charactères (sans espaces).
void chiffrer_Vigenere (wchar_t *chaine,wchar_t *cle);
```
```c
//Cette fonction permet de modifier le tableau de wchar_t pointé par chaine avec
//un déchiffrage Vigenère et sa cle.
//La clé doit être une suite de charactères (sans espaces).
void dechiffrer_Vigenere (wchar_t *chaine,wchar_t *cle);
```
```c
//Cette fonction permet de modifier le tableau de wchar_t pointé par chaine avec
//un chiffrage ou déchiffrage César et sa cle (une clé positive chiffre et une clé
//négative déchiffre).
void cesar (wchar_t *chaine,int cle);
```
#### verifEtAccents.h :
```c
//Cette fonction renvoie 1 si le tableau de wchar_t pointé par tabchar n'a pas de
//charactère spéciaux sinon elle renvoie 0.
int verifAlphaNum(wchar_t* tabchar);
```
```c
//Cette fonction prend en paramètre un tableau de wchar_t pointé par tabchar et
//permet de modifier ce tableau pour qu'il n'ai plus d'accents.
void ConvertAccents(wchar_t* tabchar);
```

## Dévelopeurs 

#### Bescond Mathis
* Vérification des symboles spéciaux et conversion des accents
* Le main
#### Colombier Korentin 
* Les algorithmes de cryptages