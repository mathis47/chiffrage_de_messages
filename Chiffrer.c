#include <stdio.h>
#include <string.h>
#include <wchar.h>
#include "Chiffrer.h"
#include <stdlib.h>


void chiffrer_Vigenere (wchar_t *chaine,wchar_t *cle){
    int t=wcslen(chaine);
    wchar_t c;
    int compt=0;
    int i =0;
    while(compt<t)
    {
        c = *chaine;
        if(c<='z' && c>='a'){
            c=(c-'a'+cle[i]-'a')%26+'a';
            *chaine=c;
            i=(i+1)%wcslen(cle);
        }
        if(c<='Z' && c>='A'){
            c=(c-'A'+cle[i]-'a')%26+'A';
            *chaine=c;
            i=(i+1)%wcslen(cle);
        }
        chaine++;
        compt++;
    }
}
void dechiffrer_Vigenere (wchar_t *chaine,wchar_t *cle){
    int t=wcslen(chaine);
    wchar_t c;
    int compt=0;
    int i =0;
    while(compt<t)
    {
        c = *chaine;
        if(c<='z' && c>='a'){
            c=(c-'a'-cle[i]+'a'+26)%26+'a';
            *chaine=c;
            i=(i+1)%wcslen(cle);
        }
        if(c<='Z' && c>='A'){
            c=(c-'A'-cle[i]+'a'+26)%26+'A';
            *chaine=c;
            i=(i+1)%wcslen(cle);
        }
        chaine++;
        compt++;
    }
}
void cesar (wchar_t *chaine,int cle){
    while(cle<0){
        cle+=26;
    }
    wchar_t *p=chaine;
    int t=wcslen(chaine);
    wchar_t c;
    int compt=0;
    while(compt<t)
    {
        c = *chaine;
        if(c<='z' && c>='a'){
            c=((c-'a')+cle)%26+'a';
            *chaine=c;
        }
        if(c<='Z' && c>='A'){
            c=((c-'A')+cle)%26+'A';
            *chaine=c;
        }
        chaine++;
        compt++;
    }
}

//test abandonnés
/*
int pgcd(int a,int b){
    int c;
    while(a!=b){
        if(a>b){
            c=a-b;
            a=c;
        }else{
            c=(b-a);
            b=c;
        }
    }
    return a;
}

void rsa_cle (int p , int q){
    int n = p*q;

    int pn =(p-1)*(q-1);

    int z;
    int e=1;
    while(z!=1){
        int c=0;
        while(c==0){
            if(p<e){
                if(q<e){
                    if(e<pn){
                        c=1;
                    }else{
                        z=1;
                        printf("Erreur\n");
                        break;
                    }
                }
            }
            e++;
            z=pgcd(e,pn);
        }
    }
    printf("%d %d\n",e,n );
}

char rsa_chiffrer(char *chaine,int e,int n){
    int t = strlen(chaine);
    char crypt[t];
    for (int i = 0; i < t; ++i)
    {
        int p = 1;
        for (int j = 0; j < e; ++j)
        {
            p=p*(chaine[i]);
            p=p%n;
        }
        crypt[i]=p;
    }
    return *crypt;
}

char rsa_dechiffrer(char *crypt,int p,int q,int e,int n,int pn){
    int d=0;
    int c=0;
    while(c==0){
        if(e*d%pn==1){
            if(p<d){
                if(q<d){
                    if(d<pn){
                        c=1;
                    }
                }
            }
        }
        d++;
    }
    d--;
    printf("%d\n",d );
    int t = strlen(crypt);
    char chaine[t];
    for (int i = 0; i < t; ++i)
    {
        int p = 1;
        for (int j = 0; j < d; ++j)
        {
            p=p*(crypt[i]);
            p=p%n;
        }
        chaine[i]=p;
    }
    return *chaine;
}
*/