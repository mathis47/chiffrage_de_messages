#include <stdio.h>
#include <string.h>
#include "verifEtAccents.h"
#include "Chiffrer.h"
#include <wchar.h>
#include <stdlib.h>
#include <locale.h>



void main(){
//setlocal
struct lconv *loc;
setlocale (LC_ALL, "");
loc=localeconv();
wprintf(L"\n---------------------\n");
wprintf(L"Chiffrage de messages\n");
wprintf(L"---------------------\n");

	//boucle principale si erreur
	int i;//variable pour savoir si on doit boucler pour une erreur de charactères spéciaux
	do
	{
		i=1;
		wchar_t tabchar[200]={0};
		wprintf(L"\nVeuillez entrer un message : ");
		fgetws(tabchar,200,stdin);
		//copie de tabchar pour l'affichage dans un fichier
		wchar_t temp[200]={0};
		wcscpy(temp,tabchar);
		
		//les clés :
		wchar_t cle1[20]={0};//Déchiffrage Vigenère
		wchar_t cle[20]={0};//Chiffrage Vigenère
		int cleCesar1;//Déchiffrage Cesar
		int cleCesar;//Chiffrage Cesar
		
		//Verification alpha numérique et Conversion des accents
		if(verifAlphaNum(tabchar)){
			ConvertAccents(tabchar);
			wprintf(L"Message à chiffrer/déchiffrer : %ls\n",tabchar );
			
			//Choix de l'algorithme__________________________________________________________________
			wchar_t choix[2]={0};
			do
			{
				wprintf(L"Choisissez l'algorithme de chiffrement : \n");
				wprintf(L"	1 - Chiffrement César\n");
				wprintf(L"	2 - Chiffrement Vigenère\n");
				wprintf(L"Choix ( 1 ou 2 ) : ");
				wscanf(L"%ls",choix);
				if (choix[0]!=L'1' && choix[0]!=L'2')
				{
					wprintf(L"\nVeuillez ressaisir votre choix\n\n");
				}
			}while (choix[0]!=L'1' && choix[0]!=L'2');
			
			//Choix de chiffrement ou déchiffrement_________________________________________________	
			wchar_t dc[2]={0};
			do
			{
				wprintf(L"\nChoisissez entre Chiffrer et Déchiffrer : \n");
				wprintf(L"	1 - Chiffrer\n");
				wprintf(L"	2 - Déchiffrer\n");
				wprintf(L"Choix ( 1 ou 2 ) : ");
				wscanf(L"%ls",dc);
				if (dc[0]!=L'1' && dc[0]!=L'2')
				{
					wprintf(L"\nVeuillez ressaisir votre choix\n");
				}
			}while (dc[0]!=L'1' && dc[0]!=L'2');

			//chiffrement césar______________________________________________________________________

			//Chiffrer_______________________________________________________________________________
			wprintf(L"\n");
			if(dc[0]==L'1' && choix[0]==L'1')
			{
				
				wprintf(L"---------------\n");
				wprintf(L"Chiffrage Cesar\n");
				wprintf(L"---------------\n\n");
				wprintf(L"Clé (décalage) : ");
				wscanf(L"%d",&cleCesar);
				cesar(tabchar,cleCesar);
				wprintf(L"\nMessage codé : %ls\n",tabchar);
			}
			//Déchiffrer_____________________________________________________________________________
			else if(dc[0]==L'2' && choix[0]==L'1')
			{	
				
				wprintf(L"-----------------\n");
				wprintf(L"Déchiffrage Cesar\n");
				wprintf(L"-----------------\n\n");
				wprintf(L"Clé (décalage) : ");
				wscanf(L"%d",&cleCesar1);
				cesar(tabchar,-cleCesar1);
				wprintf(L"\nMessage codé : %ls\n",tabchar);
			}
			
			//Chiffrement Vigenère___________________________________________________________________

			//Chiffrer_______________________________________________________________________________
			else if (dc[0]==L'1' && choix[0]==L'2')
			{
				
				wprintf(L"------------------\n");
				wprintf(L"Chiffrage Vigenère\n");
				wprintf(L"------------------\n\n");
				wprintf(L"Clé (suite de lettres) : ");
				wscanf(L"%ls",cle);
				chiffrer_Vigenere(tabchar,cle);
				wprintf(L"\nMessage codé : %ls\n",tabchar);
			}
			//Déchiffrer_____________________________________________________________________________
			else{
				
				wprintf(L"--------------------\n");
				wprintf(L"Déchiffrage Vigenère\n");
				wprintf(L"--------------------\n\n");
				wprintf(L"Clé (suite de lettres) : ");
				wscanf(L"%ls",cle1);
				dechiffrer_Vigenere(tabchar,cle1);
				wprintf(L"\nMessage codé : %ls\n",tabchar);
			}
			
			//Proposition de mettre le résultat sur un fichier_______________________________________
			wchar_t t[2]={0};//variable pour faire le choix de mettre le résultat dans le fichier
			wprintf(L"Voulez vous avoir le résultat sur un fichier texte chiffrage.txt (o/n) : ");
			wscanf(L"%ls",t);
			if (t[0]==L'o')
			{
				FILE* fichier = NULL;
				fichier = fopen("chiffrage.txt", "a");
				//affichage des différents chiffrage et clé utiliser dans le fichier
				//affichage de la clé dans le fichier
				if(dc[0]==L'1' && choix[0]==L'1'){
					fwprintf(fichier,L"\n---------------\n");
					fwprintf(fichier,L"Chiffrage Cesar\n");
					fwprintf(fichier,L"---------------\n");
					fwprintf(fichier,L"Clé : %d\n",cleCesar);
				}else if(dc[0]==L'2' && choix[0]==L'1'){
					fwprintf(fichier,L"\n-----------------\n");
					fwprintf(fichier,L"Déchiffrage Cesar\n");
					fwprintf(fichier,L"-----------------\n");
					fwprintf(fichier,L"Clé : %d\n",cleCesar1);
				}else if (dc[0]==L'1' && choix[0]==L'2'){
					fwprintf(fichier,L"\n------------------\n");
					fwprintf(fichier,L"Chiffrage Vigenère\n");
					fwprintf(fichier,L"------------------\n");
					fwprintf(fichier,L"Clé : %ls\n",cle);
				}else{
					fwprintf(fichier,L"\n--------------------\n");
					fwprintf(fichier,L"Déchiffrage Vigenère\n");
					fwprintf(fichier,L"--------------------\n");
					fwprintf(fichier,L"Clé : %ls\n",cle1);
				}
				

				//affiche du message avant/après chiffrage dans le fichier	
				fwprintf(fichier,L"Le message initial : %ls",temp);
				if (dc[0]==L'1')
				{
					fwprintf(fichier, L"Le message codé : %ls", tabchar);
				}else{
					fwprintf(fichier, L"Le message décodé : %ls", tabchar);
				}
				fclose(fichier);
				wprintf(L"Le fichier chiffrage.txt a été créé ou complété\n");
				
			}	
				wprintf(L"\nMerci de votre visite\n\n");
				wprintf(L"Bescond Mathis\n\n");
				wprintf(L"Colombier Korentin \n\n");
				
		//erreur pour les charactères spéciaux_______________________________________________________	
		}else{
			wprintf(L"Erreur le message n'est pas valide\n");
			i = 0;
		}
	//boucle principal do while en cas d'erreur de charactères spéciaux	
	}while(i==0);
	

}
