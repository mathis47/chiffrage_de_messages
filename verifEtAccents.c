#include <stdio.h>
#include <string.h>
#include <wchar.h>
#include "verifEtAccents.h"
#include <stdlib.h>
#include <wctype.h>
#define ACCENTS L"ÀÁÂÃÄÅàáâãäåÇçÈÉÊËèéêëÌÍÎÏìíîïÑñÒÓÔÕÖòóôõöÙÚÛÜùúûüÝýÿ"

int verifAlphaNum(wchar_t* tabchar){
	int a;
	a=0;
	for (int i = 0; i < wcslen(tabchar)-1; i++)
	{
		if (iswalnum(tabchar[i]))
		{
			a=1;
		}else{
			return 0;
		}
	}
	return a;
}

void ConvertAccents(wchar_t* tabchar){
	

	for (int i = 0; i < wcslen(tabchar); i++)
	{
		for (int j = 0; j < wcslen(ACCENTS) ; j++)
		{
			if (ACCENTS[j]==tabchar[i])
			{	
				switch(j)
				{
				case 0:case 1:case 2:case 3:case 4:case 5:
				tabchar[i]='A';
				break;
				case 6:case 7:case 8:case 9:case 10:case 11:
				tabchar[i]='a';
				break;
				case 12:
				tabchar[i]='C';
				break;
				case 13:
				tabchar[i]='c';
				break;
				case 14:case 15:case 16:case 17:
				tabchar[i]='E';
				break;
				case 18:case 19:case 20: case 21:
				tabchar[i]='e';
				break;
				case 22:case 23:case 24:case 25:
				tabchar[i]='C';
				break;
				case 26:case 27:case 28:case 29:
				tabchar[i]='c';
				break;
				case 30:
				tabchar[i]='N';
				break;
				case 31:
				tabchar[i]='n';
				break;
				case 32:case 33:case 34:case 35:case 36:
				tabchar[i]='O';
				break;
				case 37:case 38:case 39:case 40:case 41:
				tabchar[i]='o';
				break;
				case 42:case 43:case 44:case 45:
				tabchar[i]='U';
				break;
				case 46:case 47:case 48:case 49:
				tabchar[i]='u';
				break;
				case 50:
				tabchar[i]='Y';
				break;
				case 51:case 52:
				tabchar[i]='y';

				}
			}
		}
	}
}


