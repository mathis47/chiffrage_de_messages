#include <stddef.h>
//Cette fonction permet de modifier le tableau de wchar_t pointé par chaine avec
//un chiffrage Vigenère et sa clé.
//La clé doit être une suite de charactères (sans espaces).
void chiffrer_Vigenere (wchar_t *chaine,wchar_t *cle);
//Cette fonction permet de modifier le tableau de wchar_t pointé par chaine avec
//un déchiffrage Vigenère et sa cle.
//La clé doit être une suite de charactères (sans espaces).
void dechiffrer_Vigenere (wchar_t *chaine,wchar_t *cle);
//Cette fonction permet de modifier le tableau de wchar_t pointé par chaine avec
//un chiffrage ou déchiffrage César et sa cle (une clé positive chiffre et une clé
//négative déchiffre).
void cesar (wchar_t *chaine,int cle);



//test abandonnés
/*
void rsa_chiffrer(char *chaine,int e,int n);
void rsa_cle (int p , int q);
int pgcd(int a,int b);
char rsa_dechiffrer(char *crypt,int p,int q,int e,int n,int pn)
*/