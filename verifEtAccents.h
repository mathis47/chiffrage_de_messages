#include <stddef.h>
//Cette fonction renvoie 1 si le tableau de wchar_t pointé par tabchar n'a pas de
//charactère spéciaux sinon elle renvoie 0.
int verifAlphaNum(wchar_t* tabchar);
//Cette fonction prend en paramètre un tableau de wchar_t pointé par tabchar et
//permet de modifier ce tableau pour qu'il n'ai plus d'accents.
void ConvertAccents(wchar_t* tabchar);
